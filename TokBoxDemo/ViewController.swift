//
//  ViewController.swift
//  TokBoxDemo
//
//  Created by Duc iOS on 10/27/16.
//  Copyright © 2016 Duc iOS. All rights reserved.
//

import UIKit
import OpenTok
import SwiftyJSON
import SwiftHTTP
import SnapKit

let screenCapture = true

class ViewController: UIViewController {

    var code = ""
    var otSession: OTSession!
    var publisher: OTPublisher!
    let btnConnect: UIButton = {
        let btn = UIButton()
        btn.setTitle("Connect", for: .normal)
        btn.backgroundColor = UIColor.red
        btn.setTitleColor(UIColor.white, for: .normal)
        return btn
    }()
    let btnCamera: UIButton = {
        let btn = UIButton()
        btn.setTitle("Switch camera", for: .normal)
        btn.backgroundColor = UIColor.red
        btn.setTitleColor(UIColor.white, for: .normal)
        return btn
    }()
    lazy var cameraSession: AVCaptureSession = {
        let s = AVCaptureSession()
        s.sessionPreset = AVCaptureSessionPresetHigh
        return s
    }()
    
    var previewView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFill
        return imgView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        UIApplication.shared.isIdleTimerDisabled = true

        view.addSubview(previewView)
        previewView.snp.makeConstraints{ (make) in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        
        view.addSubview(btnConnect)
        btnConnect.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(8)
            make.right.equalTo(view).offset(-8)
            make.width.equalTo(150)
            make.height.equalTo(33)
        }
        btnConnect.addTarget(self, action: #selector(ViewController.didTapConnect(_:)), for: .touchUpInside)

        view.addSubview(btnCamera)
        btnCamera.snp.makeConstraints { (make) in
            make.bottom.equalTo(view).offset(-8)
            make.right.equalTo(view).offset(-8)
            make.width.equalTo(150)
            make.height.equalTo(33)
        }
        btnCamera.addTarget(self, action: #selector(ViewController.didTapCamera(_:)), for: .touchUpInside)
        
        view.layoutIfNeeded()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func didTapConnect(_ sender: UIButton) {
        if otSession != nil {
            cleanupPublisher()
            var error:OTError?
            otSession.disconnect(&error)
            otSession = nil
            updateBtnConnect(mode: .Connect)
        } else {
            askCode()
        }
    }
    
    func didTapCamera(_ sender: UIButton) {
        if screenCapture {
            switchCamera()
        } else {
            if publisher?.cameraPosition == .back {
                publisher?.cameraPosition = .front
            } else {
                publisher?.cameraPosition = .back
            }
        }
    }
    
    func switchCamera() {
        //Indicate that some changes will be made to the session
        cameraSession.beginConfiguration()
        
        //Remove existing input
        let currentCameraInput:AVCaptureInput = cameraSession.inputs.first as! AVCaptureInput
        cameraSession.removeInput(currentCameraInput)
        
        //Get new input
        var newCamera:AVCaptureDevice! = nil
        if let input = currentCameraInput as? AVCaptureDeviceInput {
            if (input.device.position == .back)
            {
                newCamera = cameraWithPosition(position: .front)
            }
            else
            {
                newCamera = cameraWithPosition(position: .back)
            }
        }
        
        //Add input to session
        var err: NSError?
        var newVideoInput: AVCaptureDeviceInput!
        do {
            newVideoInput = try AVCaptureDeviceInput(device: newCamera)
        } catch let err1 as NSError {
            err = err1
            newVideoInput = nil
        }
        
        if(newVideoInput == nil || err != nil)
        {
            print("Error creating capture device input: \(err!.localizedDescription)")
        }
        else
        {
            cameraSession.addInput(newVideoInput)
        }
        
        //Commit all the configuration changes at once
        cameraSession.commitConfiguration()
    }

    // Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
    func cameraWithPosition(position: AVCaptureDevicePosition) -> AVCaptureDevice?
    {
        if let devices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) {
            for device in devices {
                let device = device as! AVCaptureDevice
                if device.position == position {
                    return device
                }
            }
        }
        
        return nil
    }
    
    func askCode() {
        let alert = UIAlertController(title: "Enter code", message: nil, preferredStyle: .alert)
        alert.addTextField { (txf) in
            txf.placeholder = "Code"
            txf.keyboardType = .numberPad
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] (action) in
            self?.doConnect(code: alert.textFields!.first!.text!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    enum ConnectMode {
        case Connect, Connecting, Disconnect
    }
    
    func updateBtnConnect(mode: ConnectMode) {
        btnConnect.isEnabled = true
        switch mode {
        case .Connect:
            btnConnect.setTitle("Connect", for: .normal)
        case .Connecting:
            btnConnect.setTitle("Connecting ...", for: .normal)
            btnConnect.isEnabled = false
        case .Disconnect:
            btnConnect.setTitle("Disconnect", for: .normal)
        }
    }

    func doConnect(code: String) {
        self.code = code
        if otSession != nil {
            var error:OTError?
            otSession.disconnect(&error)
            otSession = nil
        }
        do {
            updateBtnConnect(mode: .Connecting)
            let opt = try HTTP.POST("https://lab01.dropininc.com/api/code/\(code)")
            opt.start { [weak self] response in
                DispatchQueue.main.async {
                    guard let sself = self else { return }
                    
                    if let err = response.error {
                        sself.showError(message: err.localizedDescription)
                        return //also notify app of failure as needed
                    }
                    print("opt finished: \(response.description)")
                    //print("data is: \(response.data)") access the response of the data with response.data
                    
                    let json = JSON(data: response.data)
                    if json != JSON.null {
                        let apikey = json["apiKey"].stringValue
                        let sessionId = json["sessionId"].stringValue
                        let token = json["token"].stringValue
                        
                        sself.resolution = sself.stringToResolution(string: json["resolution"].stringValue)
                        sself.frameRate = sself.stringToFrameRate(string: json["frameRate"].stringValue)
                        
                        // Initialize a new instance of OTSession and begin the connection process.
                        sself.otSession = OTSession(apiKey: apikey,
                                                    sessionId: sessionId,
                                                    delegate:self)
                        let audioDevice: OTDefaultAudioDeviceWithVolumeControl = OTDefaultAudioDeviceWithVolumeControl.sharedInstance()
                        OTAudioDeviceManager.setAudioDevice(audioDevice)
                        var error:OTError?
                        sself.otSession.connect(withToken: token, error: &error)
                        
                        if let error = error {
                            sself.showError(message: "Unable to connect to session (\(error.localizedDescription)")
                        }
                    } else {
                        sself.showError(message: "Could not get json from file, make sure that file contains valid json.")
                    }
                }
            }
        } catch let error {
            showError(message: error.localizedDescription)
        }
    }
    
    var resolution =  OTCameraCaptureResolution.high
    var frameRate = OTCameraCaptureFrameRate.rate15FPS
    
    func doPublish() {
        if publisher != nil {
            cleanupPublisher()
        }
        
        var name = ""
        switch resolution {
        case .high: name = name.appending("1280x720")
        case .medium: name = name.appending("640x480")
        default: name = name.appending("352x288")
        }
        
        switch frameRate {
        case .rate30FPS: name = name.appending(" | 30fps")
        case .rate15FPS: name = name.appending(" | 15fps")
        default: name = name.appending(" 7fps")
        }
        
        publisher = OTPublisher(delegate:self, name: name, cameraResolution: resolution, cameraFrameRate: frameRate)
        if screenCapture {
            publisher?.videoType = .screen
            publisher?.videoCapture = TBScreenCapture(view: previewView)
        } else {
            publisher?.videoType = .camera
            publisher?.cameraPosition = .back
        }
        
        var error:OTError?
        otSession.publish(publisher, error:&error)
        if let error = error {
            showError(message: "Unable to publish (\(error.localizedDescription))")
        } else {
            if screenCapture {
                setupCameraSession()
            } else {
                view.addSubview(publisher.view)
                publisher.view.frame = view.bounds
                view.sendSubview(toBack: publisher.view)
            }
        }
    }
    
    func cleanupPublisher() {
        publisher.view.removeFromSuperview()
        publisher = nil
    }
    
    func resolutionToString(resolution: OTCameraCaptureResolution) -> String {
        switch resolution {
        case .high: return "1280x720"
        case .medium: return "640x480"
        default:  return "352x288"
        }
    }
    
    func stringToResolution(string: String) -> OTCameraCaptureResolution {
        switch string {
        case "1280x720": return .high
        case "640x480": return .medium
        default: return .low
        }
    }
    
    func stringToFrameRate(string: String) -> OTCameraCaptureFrameRate {
        switch string {
        case "30": return .rate30FPS
        case "15": return .rate15FPS
        default: return .rate7FPS
        }
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .landscapeRight
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    func captureAndUploadScreenshot() {
        if let image = previewView.image, let imageData = UIImageJPEGRepresentation(image, 0.8) {
        do {
            let url = "https://lab01.dropininc.com/api/image/\(code)/save"
            let opt = try HTTP.POST(url, parameters: ["image": Upload(data: imageData, fileName: "image.jpg", mimeType: "image/jpeg")])
            opt.start { response in
                DispatchQueue.main.async {
                    if let error = response.error {
                        print("got an error: \(error)")
                        return
                    }
                    print("opt finished: \(response.description)")
                    let json = JSON(data: response.data)
                    if let err = json.error {
                        print("got an error: \(err)")
                    }
                }
            }
        } catch let error {
            print("got an error: \(error)")
        }
        }
    }
    
}

extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func setupCameraSession() {
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) as AVCaptureDevice
        do {
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            
            cameraSession.beginConfiguration()
            
            if (cameraSession.canAddInput(deviceInput) == true) {
                cameraSession.addInput(deviceInput)
            }
            
            let dataOutput = AVCaptureVideoDataOutput()
            dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)]
            dataOutput.alwaysDiscardsLateVideoFrames = true
            
            if (cameraSession.canAddOutput(dataOutput) == true) {
                cameraSession.addOutput(dataOutput)
            }
            
            cameraSession.commitConfiguration()
            
            let queue = DispatchQueue(label: "com.invasivecode.videoQueue")
            dataOutput.setSampleBufferDelegate(self, queue: queue)
            
            cameraSession.startRunning()
        }
        catch let error as NSError {
            NSLog("\(error), \(error.localizedDescription)")
        }
    }
    
    func setupCameraSession() {
        let devices = AVCaptureDevice.devices()
        var captureDevice:AVCaptureDevice?
        
        do {
            if cameraType == CameraType.Front {
                for device in devices {
                    if device.position == AVCaptureDevicePosition.Front {
                        captureDevice = device as? AVCaptureDevice
                        break
                    }
                }
            }
            else {
                captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo) as AVCaptureDevice
            }
            
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            
            cameraSession.beginConfiguration()
            
            if (cameraSession.canAddInput(deviceInput) == true) {
                cameraSession.addInput(deviceInput)
            }
            
            let dataOutput = AVCaptureVideoDataOutput()
            dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(unsignedInt: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
            dataOutput.alwaysDiscardsLateVideoFrames = true
            dataOutput.setSampleBufferDelegate(self, queue: dispatch_queue_create("cameraQueue", DISPATCH_QUEUE_SERIAL))
            
            if (cameraSession.canAddOutput(dataOutput) == true) {
                cameraSession.addOutput(dataOutput)
            }
            
            cameraSession.commitConfiguration()
            
        }
        catch let error as NSError {
            NSLog("\(error), \(error.localizedDescription)")
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        // Here you collect each frame and process it
        DispatchQueue.main.async {
            self.previewView.image = self.imageFromSampleBuffer(sampleBuffer: sampleBuffer)
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didDrop sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        // Here you can count how many frames are dopped
    }
    
    func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer) -> UIImage? {
        if let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) {
            CVPixelBufferLockBaseAddress(imageBuffer,CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
            let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)
            let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)
            let width = CVPixelBufferGetWidth(imageBuffer)
            let height = CVPixelBufferGetHeight(imageBuffer)
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            if let context = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue) {
                
                let quartzImage = context.makeImage()
                CVPixelBufferUnlockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
                
                if let quartzImage = quartzImage {
                    //let image = UIImage(cgImage: quartzImage)
                
                    var orientation = UIImageOrientation.up
                    if let input = cameraSession.inputs.first as? AVCaptureDeviceInput, input.device.position == .front {
                        orientation = .downMirrored
                    }
                    
                    let image = UIImage(cgImage: quartzImage, scale: 1.0, orientation: orientation)
                    return image
                }
            }
        }
        return nil
    }
}

extension ViewController: OTSessionDelegate {
    func session(_ session: OTSession!, didFailWithError error: OTError!) {
        //
    }
    func session(_ session: OTSession!, streamCreated stream: OTStream!) {
        //
    }
    func session(_ session: OTSession!, streamDestroyed stream: OTStream!) {
        cleanupPublisher()
    }
    func sessionDidConnect(_ session: OTSession!) {
        // We have successfully connected, now start pushing an audio-video stream
        // to the OpenTok session.
        updateBtnConnect(mode: .Disconnect)
        doPublish()
    }
    func sessionDidDisconnect(_ session: OTSession!) {
        //
    }
    func sessionDidReconnect(_ session: OTSession!) {
        //
    }
    func sessionDidBeginReconnecting(_ session: OTSession!) {
        //
    }
    func session(_ session: OTSession!, receivedSignalType type: String!, from connection: OTConnection!, with string: String!) {
        switch type {
        case "resolutionChanged":
            let data = string.data(using: .utf8)!
            let json = JSON(data: data)
            if json != JSON.null {
                let resolution = json["resolution"].stringValue
                let frameRate = json["frameRate"].stringValue
                print("resolution: \(resolution) frameRate: \(frameRate)")
                
                self.resolution = stringToResolution(string: resolution)
                self.frameRate = stringToFrameRate(string: frameRate)
                
                doPublish()
            } else {
                showError(message: "Could not get json from file, make sure that file contains valid json.")
            }
            
        case "captureScreenshot":
            self.captureAndUploadScreenshot()
        default: break
        }
    }
    
    func showError(message: String) {
        let alert = UIAlertController(title: "ERROR", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)

        updateBtnConnect(mode: .Connect)
    }
    
}

extension ViewController: OTPublisherDelegate {
    /**
     * Sent if the publisher encounters an error. After this message is sent,
     * the publisher can be considered fully detached from a session and may
     * be released.
     * @param publisher The publisher that signalled this event.
     * @param error The error (an <OTError> object). The `OTPublisherErrorCode` 
     * enum (defined in the OTError class)
     * defines values for the `code` property of this object.
     */
    public func publisher(_ publisher: OTPublisherKit!, didFailWithError error: OTError!) {
        //
    }

    func publisher(_ publisher: OTPublisher!, didChangeCameraPosition position: AVCaptureDevicePosition) {
        //
    }
}
